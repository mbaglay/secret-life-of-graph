package ua.mbaglay;

/**
 * Created by maxim on 24.08.17.
 */
public class GraphNode {
    protected int index;

    public GraphNode() {
        this.index = -1;
    }

    public GraphNode(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
