package ua.mbaglay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by maxim on 24.08.17.
 */
public class GraphSearchAStar<NT extends NavGraphNode, ET extends GraphEdge, GT extends SparseGraph<NT, ET>, H extends Heuristic> {
    private GT graph;
    private List<Double> gCosts;
    private List<Double> fCosts;
    private List<ET> shortestPathTree;
    private List<ET> searchFrontier;

    private int indexSource;
    private int indexTarget;

    public GraphSearchAStar(GT graph, int indexSource, int indexTarget, H heuristic) {
        this.graph = graph;
        this.indexSource = indexSource;
        this.indexTarget = indexTarget;
        this.shortestPathTree = new ArrayList<ET>(graph.numberOfNodes());
        this.searchFrontier = new ArrayList<ET>(graph.numberOfNodes());
        this.gCosts = new ArrayList<Double>(Collections.nCopies(graph.numberOfNodes(), 0.0));
        this.fCosts = new ArrayList<Double>(Collections.nCopies(graph.numberOfNodes(), 0.0));

        search(heuristic);
    }

    private void search(H heuristic) {
        PriorityQueue.IndexedPriorityQLow<Double> priorityQLow = new PriorityQueue.IndexedPriorityQLow<Double>(
                this.fCosts,
                graph.numberOfNodes());

        priorityQLow.insert(indexSource);

        while (!priorityQLow.empty()) {
            int nextClosestNode = priorityQLow.Pop();

            this.shortestPathTree.add(nextClosestNode, this.searchFrontier.get(nextClosestNode));

            if (nextClosestNode == indexTarget) {
                return;
            }

            for (ET edge: this.graph.getListOfEdgesForm(nextClosestNode)) {
                double hCost = heuristic.calculate(this.graph, indexTarget, edge.getIndexTo());
                double gCost = this.gCosts.get(nextClosestNode) + edge.getCost();

                if (this.searchFrontier.get(edge.getIndexTo()) == null) {
                    this.fCosts.add(edge.getIndexTo(), gCost + hCost);
                    this.gCosts.add(edge.getIndexTo(), gCost);

                    priorityQLow.insert(edge.getIndexTo());

                    this.searchFrontier.add(edge.getIndexTo(), edge);
                } else if ((gCost < this.gCosts.get(edge.getIndexTo()))
                        && (this.shortestPathTree.get(edge.getIndexTo()) == null)) {
                    this.fCosts.add(edge.getIndexTo(), gCost + hCost);
                    this.gCosts.add(edge.getIndexTo(), gCost);
                    priorityQLow.ChangePriority(edge.getIndexTo());
                    this.searchFrontier.add(edge.getIndexTo(), edge);
                }
            }
        }
    }

    public List<ET> getSPT() {
        return this.shortestPathTree;
    }

    public List<Integer> getPathToTarget() {
        List<Integer> path = new LinkedList<Integer>();

        if ( indexTarget < 0) {
            return path;
        }

        int ind = indexTarget;

        path.add(ind);

        while (ind != indexSource && this.shortestPathTree.get(ind) != null) {
            ind = this.shortestPathTree.get(ind).getIndexFrom();
            path.add(ind);
        }

        Collections.reverse(path);

        return path;
    }

    public double getCostToTarget() {
        return this.gCosts.get(indexTarget);
    }
}
