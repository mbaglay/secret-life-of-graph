package ua.mbaglay;

import java.util.List;

/**
 * Created by maxim on 24.08.17.
 */
public class SparseGraph<NT extends NavGraphNode, ET extends GraphEdge> {
    private List<NT> nodes;
    private List<List<ET>> edges;
    private boolean directed;
    private int nextNodeIndex;

    public SparseGraph(boolean directed) {
        this.directed = directed;
    }

    public NT getNode(int index) {
        for (NT node: nodes) {
            if (node.getIndex() == index) {
                return node;
            }
        }

        return null;
    }

    public List<ET> getListOfEdgesForm(int indexFrom) {
        return edges.get(indexFrom);
    }

    public ET getEdge(int indexFrom, int indexTo) {
        for (List<ET> edgesInNode: edges) {
            for(ET edge: edgesInNode) {
                if (edge.getIndexFrom() == indexFrom &&
                        edge.getIndexTo() == indexTo) {
                    return edge;
                }
            }
        }

        return null;
    }

    public int getNextFreeNodeIndex() {
        return ++nextNodeIndex;
    }

    public int addNode(NT node) {
        node.setIndex(this.getNextFreeNodeIndex());
        this.nodes.add(node);

        return node.getIndex();
    }

    public void removeNode(int index) {
        for (NT node: nodes) {
            if (node.getIndex() == index) {
                nodes.remove(node);
            }
        }
    }

    public void addEdge(ET edge) {
        this.edges.get(edge.getIndexFrom()).add(edge);
    }

    public void removeEdge(int indexFrom, int indexTo) {
        for (List<ET> edgesInNode: edges) {
            for(ET edge: edgesInNode) {
                if (edge.getIndexFrom() == indexFrom &&
                        edge.getIndexTo() == indexTo) {
                    edgesInNode.remove(edge);
                }
            }
        }
    }

    public int numberOfNodes() {
        return this.nodes.size();
    }

    public int numberOfActiveNodes() {
        return this.nodes.size();
    }

    public int numberOfEdges() {
        int numberOfEdges = 0;

        for (List<ET> edgesInNode: edges) {
            numberOfEdges += edgesInNode.size();
        }

        return numberOfEdges;
    }

    public boolean isDirected() {
        return this.directed;
    }

    public boolean isEmpty() {
        return this.nodes.isEmpty();
    }

    public boolean isPresent(int index) {
        for (NT node: nodes) {
            if (node.getIndex() == index) {
                return true;
            }
        }

        return false;
    }

    public void clear() {
        this.nodes.clear();
        this.edges.clear();
    }
}
