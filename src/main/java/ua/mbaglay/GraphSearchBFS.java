package ua.mbaglay;

import java.util.*;
import java.util.concurrent.SynchronousQueue;

/**
 * Created by maxim on 24.08.17.
 */
public class GraphSearchBFS<NT extends NavGraphNode, ET extends GraphEdge, GT extends SparseGraph<NT, ET>> {
    private enum state {
        visited, unvisited, no_parent_assigned
    }

    private GT graph;
    private List<GraphSearchBFS.state> visited;
    private List<Integer> route;

    private int indexSource;
    private int indexTarget;

    private boolean found;

    public GraphSearchBFS(GT graph, int indexSource, int indexTarget) {
        this.graph = graph;
        this.indexSource = indexSource;
        this.indexTarget = indexTarget;
        this.found = false;
        this.visited = new Vector<GraphSearchBFS.state>(Collections.nCopies(graph.numberOfNodes(), GraphSearchBFS.state.unvisited));
        this.route = new Vector<Integer>(Collections.nCopies(graph.numberOfNodes(), 0));
        this.found = search();
    }

    private boolean search() {
        Queue<ET> queue = new SynchronousQueue<ET>();

        GraphEdge dummy = new GraphEdge(indexSource, indexTarget, 0);

        queue.add((ET) dummy);

        this.visited.add(indexSource, state.visited);

        while (queue.size() > 0) {
            ET next = queue.peek();
            queue.remove();
            route.add(next.getIndexTo(), next.getIndexFrom());

            if (next.getIndexTo() == indexTarget) {
                return true;
            }

            for (ET edge: this.graph.getListOfEdgesForm(next.getIndexTo())) {
                if (visited.get(edge.getIndexTo()).equals(GraphSearchBFS.state.unvisited)) {
                    queue.add(edge);
                    this.visited.add(edge.getIndexTo(), state.visited);
                }
            }
        }

        return false;
    }

    public boolean isFound() {
        return found;
    }

    public List<Integer> getPathToTarget() {
        List<Integer> path = new LinkedList<Integer>();

        if (!found || indexTarget < 0) {
            return path;
        }

        int ind = indexTarget;

        path.add(ind);

        while (ind != indexSource) {
            ind = route.get(ind);
            path.add(ind);
        }

        Collections.reverse(path);

        return path;
    }
}
