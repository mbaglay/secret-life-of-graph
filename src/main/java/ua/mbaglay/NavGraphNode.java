package ua.mbaglay;

import ua.mbaglay.geometry.Vector2D;

/**
 * Created by maxim on 24.08.17.
 */
public class NavGraphNode<T> extends GraphNode {
    protected Vector2D position;
    protected T extraInfo;
}
