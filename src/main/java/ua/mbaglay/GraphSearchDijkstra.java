package ua.mbaglay;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

/**
 * Created by maxim on 24.08.17.
 */
public class GraphSearchDijkstra<NT extends NavGraphNode, ET extends GraphEdge, GT extends SparseGraph<NT, ET>> {

    private GT graph;

    private List<ET> shortestPathTree;

    private List<Double> costToThisNode;

    private List<ET> searchFrontier;

    private int indexSource;
    private int indexTarget;

    public GraphSearchDijkstra(GT graph, int indexSource, int indexTarget) {
        this.graph = graph;
        this.indexSource = indexSource;
        this.indexTarget = indexTarget;
        this.shortestPathTree = new Vector<ET>(graph.numberOfNodes());
        this.searchFrontier = new Vector<ET>(graph.numberOfNodes());
        this.costToThisNode = new Vector<Double>(graph.numberOfNodes());

        search();
    }

    private void search() {
        PriorityQueue.IndexedPriorityQLow<Double> priorityQLow = new PriorityQueue.IndexedPriorityQLow<Double>(
                this.costToThisNode,
                graph.numberOfNodes());

        priorityQLow.insert(indexSource);

        while (!priorityQLow.empty()) {
            int nextClosestNode = priorityQLow.Pop();

            this.shortestPathTree.add(nextClosestNode, this.searchFrontier.get(nextClosestNode));

            if (nextClosestNode == indexTarget) {
                return;
            }

            for (ET edge: this.graph.getListOfEdgesForm(nextClosestNode)) {
                double newCost = this.costToThisNode.get(nextClosestNode) + edge.getCost();

                if (this.searchFrontier.get(edge.getIndexTo()) == null) {
                    this.costToThisNode.add(edge.getIndexTo(), newCost);

                    priorityQLow.insert(edge.getIndexTo());

                    this.searchFrontier.add(edge.getIndexTo(), edge);
                } else if ((newCost < this.costToThisNode.get(edge.getIndexTo()))
                        && (this.shortestPathTree.get(edge.getIndexTo()) == null)) {
                    this.costToThisNode.add(edge.getIndexTo(), newCost);
                    priorityQLow.ChangePriority(edge.getIndexTo());
                    this.searchFrontier.add(edge.getIndexTo(), edge);
                }
            }
        }
    }

    public List<ET> getAllPaths() {
        return this.shortestPathTree;
    }

    public List<Integer> getPathToTarget() {
        List<Integer> path = new LinkedList<Integer>();

        if ( indexTarget < 0) {
            return path;
        }

        int ind = indexTarget;

        path.add(ind);

        while (ind != indexSource && this.shortestPathTree.get(ind) != null) {
            ind = this.shortestPathTree.get(ind).getIndexFrom();
            path.add(ind);
        }

        Collections.reverse(path);

        return path;
    }

    public double getCostToTarget() {
        return this.costToThisNode.get(indexTarget);
    }
}
