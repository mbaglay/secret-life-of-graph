package ua.mbaglay;

import java.util.*;

/**
 * Created by maxim on 24.08.17.
 */
public class GraphSearchDFS<NT extends NavGraphNode, ET extends GraphEdge, GT extends SparseGraph<NT, ET>> {
    private enum state {
        visited, unvisited, no_parent_assigned
    }

    private GT graph;
    private List<state> visited;
    private List<Integer> route;

    private int indexSource;
    private int indexTarget;

    private boolean found;

    public GraphSearchDFS(GT graph, int indexSource, int indexTarget) {
        this.graph = graph;
        this.indexSource = indexSource;
        this.indexTarget = indexTarget;
        this.found = false;
        this.visited = new Vector<state>(Collections.nCopies(graph.numberOfNodes(), state.unvisited));
        this.route = new Vector<Integer>(Collections.nCopies(graph.numberOfNodes(), 0));
        this.found = search();
    }

    private boolean search() {
        Stack<ET> stack = new Stack<ET>();

        GraphEdge dummy = new GraphEdge(indexSource, indexTarget, 0);

        stack.add((ET) dummy);

        while (!stack.empty()) {
            ET next = stack.pop();
            route.add(next.getIndexTo(), next.getIndexFrom());
            visited.add(next.getIndexTo(), state.visited);

            if (next.getIndexTo() == indexTarget) {
                return true;
            }

            for (ET edge: this.graph.getListOfEdgesForm(next.getIndexTo())) {
                if (visited.get(edge.getIndexTo()).equals(state.unvisited)) {
                    stack.push(edge);
                }
            }
        }

        return false;
    }

    public boolean isFound() {
        return found;
    }

    public List<Integer> getPathToTarget() {
        List<Integer> path = new LinkedList<Integer>();

        if (!found || indexTarget < 0) {
            return path;
        }

        int ind = indexTarget;

        path.add(ind);

        while (ind != indexSource) {
            ind = route.get(ind);
            path.add(ind);
        }

        Collections.reverse(path);

        return path;
    }
}
