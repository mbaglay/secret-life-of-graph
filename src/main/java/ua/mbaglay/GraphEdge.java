package ua.mbaglay;

/**
 * Created by maxim on 24.08.17.
 */
public class GraphEdge {
    protected int indexFrom;
    protected int indexTo;

    protected double cost;

    public GraphEdge() {
        this(-1, -1);
    }

    public GraphEdge(int indexFrom, int indexTo) {
        this(indexFrom, indexTo, 1.0);
    }

    public GraphEdge(int indexFrom, int indexTo, double cost) {
        this.indexFrom = indexFrom;
        this.indexTo = indexTo;
        this.cost = cost;
    }

    public int getIndexFrom() {
        return indexFrom;
    }

    public void setIndexFrom(int indexFrom) {
        this.indexFrom = indexFrom;
    }

    public int getIndexTo() {
        return indexTo;
    }

    public void setIndexTo(int indexTo) {
        this.indexTo = indexTo;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
