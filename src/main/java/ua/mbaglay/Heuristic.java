package ua.mbaglay;

/**
 * Created by maxim on 24.08.17.
 */
public interface Heuristic<GT extends SparseGraph> {
    double calculate(GT graph, int index1, int index2);
}
